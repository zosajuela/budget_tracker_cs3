import styles from '../components/Styles/Reg.module.css';
import { motion } from 'framer-motion';
import { useState, useEffect } from 'react';
import AppHelper from '../apphelper';
import Link from "next/link";
import { useRouter } from 'next/router';
import Swal from 'sweetalert2'

const containerVariants = {
    hidden: {
        x: '-100vw',
    },
    visible: {
        x: 0,
        transition: {
            delay: 0.5, duration: 0.5
        }
    },
    exit: {
        x: '-100vw',
        transition: {
            ease: 'easeInOut'
        }
    }
}

 const register = () => {
    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [verifypassword, setVerifypassword] = useState("")
    const [mobileNo, setMobileNo] = useState("")
    const [active, setActive] = useState(false)
    const [typePass, setTypePass] = useState(false)
    const route = useRouter();

    useEffect(() => {
    
        if(firstName.lenth === 0 || lastName.length === 0 || email.length === 0 || password.length === 0 || verifypassword === 0 || password != verifypassword || mobileNo.length === 0){
            setActive(true);
       }else{
            setActive(false);
       }
    },[firstName, lastName, email, password, verifypassword, mobileNo])
    
    useEffect(()=> {
		let userToken = localStorage.getItem("token")
	if(userToken){
		
	  return route.push("/");
	}
	},[route])

    
    const registerUser = (e) => {
        e.preventDefault()
    
///
fetch(`${AppHelper.API_URL}/users/email-exists`, {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        email: email
    }) //convert the value to string data type to be accepted in the API
}).then(res => res.json()).then(data => {
   //lets create a checker just to see if may nakukuha tayu na data
   console.log(data)
   //lets create a control structure to give the appropriate response according to the value of the data. 
   if(data === false){

///

        const payload = {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    mobileNo: mobileNo,
                    password: password
                })
        }
    
        fetch(`${AppHelper.API_URL}/users/register`, payload)
            .then(res => res.json())
            .then(data => {
                if(data == true){
                    Swal.fire({
                        icon: "success",
                        title: "Successfully Registered",
                        text: "Thank you for registering."
                    })

                    location.reload()
                }else{
                    Swal.fire({
                        icon: "error",
                        title: "Registration failed",
                        text: "Something went wrong"
                    })

                }
            })
        } else {
            //the else branch will run if the return value is true
            Swal.fire({
                icon: 'error',
                title: 'Registration Failed',
                text: 'Email is already taken by someone else, Move on!'
            })

        }
  })
} 

    
    return (
        <div>
            <div className={styles.margin}></div>
            <motion.div className={styles.container}
                      variants={containerVariants}
                        initial="hidden"
                         animate="visible"
                        exit="exit">


                    <form className={styles.login_box}
                          onSubmit={(e) => registerUser(e)}>
                          <input 
                          className={styles.username}
                          value={firstName} 
                          placeholder="first name"
                          onChange={(e) => setFirstName(e.target.value)}
                          />
                          <input 
                          className={styles.username}
                          value={lastName} 
                          placeholder="last name"
                          onChange={(e) => setLastName(e.target.value)}
                          />
                        <input 
                        className={styles.username}
                        value={email} 
                        placeholder="Email"
                        onChange={(e) => setEmail(e.target.value)}
                        />
                        <input 
                        className={styles.password}
                        type={typePass ? "text" : "password"}
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        />

                        <div className={styles.center}>
                        <input className={styles.tick} 
                        type="checkbox" 
                        name=""
                        onClick={() => setTypePass(!typePass)}/>
                        </div>
                        <div className={styles.small}>{typePass ? 'Hide' : 'Show'}</div>

                        <input 
                        className={styles.password}
                        type={typePass ? "text" : "password"}
                        placeholder="verify Password"
                        value={verifypassword}
                        onChange={(e) => setVerifypassword(e.target.value)}
                        />
                       
                        <input 
                        className={styles.password}
                        type="number" 
                        placeholder="mobile"
                        value={mobileNo}
                        onChange={(e) => setMobileNo(e.target.value)}/>

                        <input 
                        className={styles.btn}
                        variant="primary" 
                        type="submit" 
                        disabled={active}
                        />

                        <div className={styles.link}>
                        <Link href="/login">
                            <a>Back to Login</a>
                        </Link>
                    </div>
                    </form>
                   
            </motion.div>
        </div>
    )
}

export default register
