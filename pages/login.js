import {useState, useEffect, useContext} from 'react';
import AppHelper from '../apphelper';
import { motion } from 'framer-motion';
import UserContext from '../UserContext'
import { GoogleLogin } from 'react-google-login';
// import { useRouter } from 'next/router';
import styles from '../components/Styles/Entry.module.css';
import Link from "next/link";
import { useRouter } from 'next/router';
import Swal from 'sweetalert2';

export default function login(props){

	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [disable, setDisable] = useState(true) 
	const [typePass, setTypePass] = useState(false) 
	const route = useRouter();
	
	

	useEffect(()=> {
		if(email !== "" && password !== ""){
			setDisable(false) //enabling the login button
		} else {
			setDisable(true) //disable the login button
		}
	},[email, password])

  
	useEffect(()=> {
		let userToken = localStorage.getItem("token")
	if(userToken){
	  return route.push("/");
	}
	},[route])


	const userLogin = (e) => {
		e.preventDefault()
		const payload = {
			method: "POST",
			headers: {
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}

		fetch(`${AppHelper.API_URL}/users/login`, payload)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(data !== false){
				localStorage.setItem('token', data.access)
				location.reload();
				Swal.fire({
					icon: "success",
					title: "Login Successful",
					text: "You may now log your budget!"
				})
				
				const payload = {
					method: "GET",
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${data.access}`
					}
				}

				fetch(`${AppHelper.API_URL}/users/details`, payload)
				.then(res => res.json())
				.then(data => console.log(data))
				.catch(error => console.log(error))

			} else {
				Swal.fire({
					icon: "error",
					title: "Wrong Credentials, try another one!",
					text: "Something went wrong"
				})
			}
		})
		.catch(error => {
			console.log(error)
		})
	}


	const retrieveGoogleDetails = (response) => {
			console.log(response.tokenId)

			const payload = {
				method: "POST",
				headers: {'Content-Type': 'application/json'},
				body: JSON.stringify({ tokenId: response.tokenId })
			}
			fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload)
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(!data || localStorage !== ""){
					localStorage.setItem("token", data.access)
					location.reload();
					Swal.fire({
						icon: "success",
						title: "Googl-Login Successful",
						text: "You may now log your budget!"
					})
					
				}else{
					localStorage.setItem("token", data.access)
				
				}
			})
	}

	const containerVariants = {
        hidden: {
            x: '-100vw',
        },
        visible: {
            x: 0,
            transition: {
                delay: 0.5, duration: 0.5
            }
        },
        exit: {
            x: '-100vw',
            transition: {
                ease: 'easeInOut'
            }
        }
    }


	return(
		<div>
		<div className={styles.margin}></div>
		
		 <motion.div 
		 className={styles.container}
		 variants={containerVariants}
		 initial="hidden"
		 animate="visible"
		 exit="exit"
		 >

		 <form className={styles.login_box} onSubmit={e => userLogin(e)}>
		 <p>Please Login to </p>
		 <p>Budget you C🤑$H </p>
		 <input 
		 className={styles.username} 
		 type="email"
		 placeholder="Enter email" 
		 value={email}
		 onChange={(e) => setEmail(e.target.value)}
		  />

		  <div className={styles.center}>
		  <input className={styles.tick} 
		  type="checkbox" 
		  name=""
		  onClick={() => setTypePass(!typePass)}/>
		  </div>
		  <div className={styles.small}>{typePass ? 'Hide' : 'Show'}</div>


		 <input 
		 className={styles.password}
		  type="password" 
		  placeholder="Password" 
		  type={typePass ? "text" : "password"}
		 onChange={(e)=> setPassword(e.target.value)} 
		  />
		 <input 
		 type="submit"  
		 className={styles.btn} 
		 value="login" 
		 disabled={disable}/>
		
		 <div className={styles.google}>
			 <GoogleLogin
			 buttonText="sign in with google"
			 clientId="59314701399-35m800pe67snb4repi9k82o72a3muprj.apps.googleusercontent.com"
			 onSuccess={retrieveGoogleDetails}
			 onFailure={retrieveGoogleDetails}
			 cookiePolicy={'single_host_origin'}
			 theme="dark"
			 
			 />
		 </div>
			 <div className={styles.link}>
				 <Link href="/register">
					 <a>Register Now</a>
				 </Link>
			 </div>
		 </form>
		 <br/>
				  </motion.div>
			   
	 </div>
		)
}
