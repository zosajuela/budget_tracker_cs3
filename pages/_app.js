import {useState, useEffect} from 'react';
import '../styles/globals.css';
import Layout from '../components/Layout/Layout'

import {UserProvider} from '../UserContext';
import AppHelper from '../apphelper'


function MyApp({ Component, pageProps }) {
	const [user, setUser] = useState({
		email: null,
		isAdmin: null,
		id: null
	})
	const unsetUser = () => {
		localStorage.clear()
		setUser({
			email: null,
			isAdmin: null,
			id: null
		})
	}

	useEffect(() => {
		if(localStorage.getItem('token')){
			const payload = {
				headers: {Authorization: `Bearer${AppHelper.getAccessToken()}`}
			}
			fetch(`{AppHelper.API_URL}/users/details`, payload)
			.then(res => res.json())
			.then(data => {
				setUser({
					email: data.email,
					isAdmin: data.isAdmin,
					id: data._id
			})
		})
			.catch(error => console.log(error))
		} else {
			console.log("No Authenticated User")
		}
	},[user.id])

  return (
  	<div>
		<Layout />
	  <UserProvider value={{ user, setUser, unsetUser }}>
			<Component {...pageProps} />
	  </UserProvider>
	</div>
  	)

}

export default MyApp
